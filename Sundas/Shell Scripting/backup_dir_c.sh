cd ~
#check for existence of backup  directory
if [ -d $1 ];      
 then
 echo "Directory Exists"
# findind .c files in directory imp files
CFILES_FOUND=$(find /home/emumba/Imp_Files -type f -name "*.c")
# loop to access each found file individually
for i in $CFILES_FOUND;do
 bsname=$(basename $i)
#check if file exists in backup dir
  if [ -f "/home/emumba/"$1"/"$bsname"" ];
    then
    echo ""$bsname" already exists"
    diff $i "/home/emumba/"$1"/"$bsname"" > /dev/null
    if [ $? -eq 0 ];
     then
      echo "same file....not modified since last backup"
    else 
      echo  "file is modified.....copying updated file to backup directory"
      cp -u $i /home/emumba/$1
      echo "backed up"
    fi
# making copy of file in backup directory
  else
    echo ""$bsname" had no previous copy"
    cp $i /home/emumba/$1
    echo "now backed up"
  fi
done
 echo "files are successfully backed up and updated"
# making backup directory and copying .c files in it
else  echo "directory doesnot exist"
      echo "making directory"
      mkdir /home/emumba/$1
      find /home/emumba/Imp_Files -type f -name '*.c' -exec  cp {} /home/emumba/$1 \;
      echo "files are copied to backup directory"
fi

