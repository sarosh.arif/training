# Introduction

Repo to run cmake using docker image.

This directory contains two shell scripts build.sh and run.sh.

build.sh is to build docker image from Dockerfile.

run.sh is to run docker container that mounts current working directory in container.

Dockerfile contains commands to build docker image.

Once the docker container is running, current working directory is mounted that conatins CMakeLists.txt file.

Then cmake and make is used to build from this cmake file.

# Building

Write following commands on terminal.

```bash
./build.sh
./run.sh  
#docker starts running  
cd /usr/src/dockercmake
mkdir build
cd build
cmake .. -DCMAKE_BUILD_TYPE=Release
make
```

# Running

myapp contains source file and CMakeLists for making application.

mylib contains source file and CMakeLists for making student library.

After building executables are placed in bin folder.

lib folder contains libraries built using cmake excluding third party libraries.

include folder contains header for student library.

Third party libraries will be in build/external/lib.

Headers files of third party libraries will be in build/external/include.

To run executables:
```bash
cd ../bin
./studentapp
./Student_bench
./Student_gtest
```
## Important Note:
Internet connection is required as third party libraries will be downloaded from github.
