coordinates= { "x":0, "y":0 }
for x in range(4):
    direction,steps=raw_input().split()
    if (direction == "UP"):
        coordinates["y"] += int(steps)
    elif (direction == "DOWN"):
        coordinates["y"] -= int(steps)
    elif (direction == "LEFT"):
        coordinates["x"] -= int(steps)
    elif (direction == "RIGHT"):
        coordinates["x"] += int(steps)
distance = (coordinates["x"]**2 + coordinates["y"]**2)**0.5
print(int(distance))
