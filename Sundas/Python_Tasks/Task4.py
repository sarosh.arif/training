import random
circle_points=0
square_points=0
origin_dist=0
radius=180
i=10
l=raw_input("Enter option -i: no of iterations, -h:help ,-j:from JSON")
if(l=="-i"):
    i=int(raw_input("enter no of iterations:"))
elif(l=="-h"):
    print "-i : for entering no of iterations by user\n  -j : for getting no of iterations from json file"
elif(l=="-j"):
    object_json=open("/home/emumba/Python_Tasks/iterations.json","r+")
    i=int(object_json.read())
for x in range(0,i):
    rand_x=random.randint(0,radius)
    rand_y=random.randint(0,radius)
    origin_dist=(rand_x**2 + rand_y**2)**0.5
    print(origin_dist)
    if (origin_dist <= radius):
        circle_points+=1
    square_points+=1
    print('circle points:' + str(circle_points))
    print('square points:' + str(square_points))
pi1=float(circle_points)/float(square_points)
pi = 4*pi1
print('pi:'+ str(pi))
