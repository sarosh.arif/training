for {set i 1} {$i <= 100 } {incr i} {
        if {$i % 3 == 0 && $i % 5 == 0} {
            puts "onoff"
        } elseif {$i % 3 == 0} {
            puts "on"
        } elseif {$i % 5 == 0} {
            puts "off"
        } else {
            puts $i
        }
    }