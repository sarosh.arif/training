proc sum_no {n} {
    if {$n<10} {
        return $n
    } else {
        return [expr [expr $n%10] + [sum_no [expr $n/10]]]
    }
}
proc sum_ten {no} {
    if {$no<10} {
        return $no
    } else {
        return [sum_ten [sum_no $no]]
    }
}
gets stdin no
puts [sum_ten $no]