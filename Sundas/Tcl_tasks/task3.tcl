proc fib {n} {
    set no1 0
    set no2 1
    set sum 0
    if { $n == 1} {
        set sum 1
    } else {
    for
     {set i 0} {$i < [expr $n -1]} {incr i} {
        set sum [expr $no1+$no2]
        set no1 $no2
        set no2 $sum   
    }
    }
    return $sum
}
gets stdin no
puts [fib $no]