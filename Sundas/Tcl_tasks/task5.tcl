proc bin n { 
    set rem 0
    set dec 1
    while {$n>0} {
        set rem [expr {$n%2 * $dec + $rem}]
        set n [expr {$n/2}]
        set dec [expr {$dec * 10}]     
    }
    return $rem
}
proc binary no {
    set remainder {} 
    while {$no>0} {
        set remainder [expr {$no%2}]$remainder
        set no [expr {$no/2}]
    }
    if {$remainder == {}} {set remainder 0}
    return $remainder
}
gets stdin n
puts [bin $n]
puts [binary $n]