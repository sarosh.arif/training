# CMake task

## 1. Introduction

There are two folders. "Demo-Setup" contains the CMake task. "dockertask" contains a docker file which is used to run the CMake task.

## 2. Downloading

To download this repository, use the following command:

```shell
git clone --recursive https://gitlab.com/sarosh.arif/training.git
```

This will download the complete training repository along with the required third party libraries.

## 3. Building Docker Image

First build docker image using either of the following two methods:

### Building Docker Image: Using commands

Run the following commands to build docker image:

```shell
cd path_of_training_repository/saad/CMake_task/dockertask
docker build -t cmaketask:1.0 .
```

### Building Docker Image: Using shell script

Run the shell script "dockerbuild.sh" to build docker image:

```shell
./dockerbuild.sh
```

## 4. Running Docker Image

Once the docker image is built, then run the image using either of the following two methods:

### Running Docker Image: Using commands

Run the following commands to run docker image:

```shell
docker run --name cmaketask -ti -u $(id -u):$(id -g) \
-v path_of_folder_Demo-Setup_on_host:/usr/src/cmaketask cmaketask:1.0
```

### Running Docker Image: Using shell script

Run the shell script "dockerrun.sh" to run docker image:

```shell
./dockerrun.sh
```

## 5. Building CMake Project

Note: Make sure that the third party libraries have been download in the folder third_party_lib. If not, then use instructions provided in "2. Downloading" to download the necessary files.
Once docker runs, use the following commands to build the project:

```shell
cd path_of_training_repository/saad/CMake_task/Demo-Setup
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make
```

## 6. Running CMake Project

The executable files are generated in 'path_of_training_repository/saad/CMake_task/Demo-Setup/build/bin'. To run them individually, use the following commands:

```shell
./benchmark
./googletest
./mainapp
```
