#!/usr/bin/env bash
docker ps -a | grep cmaketask &> /dev/null #Check if container cmaketask already exists.
if [ $? -eq 0 ] #$?=1 if cmaketask not found. $?=0 if cmaketask found
then
 if [ "$(docker inspect -f {{.State.Running}} cmaketask)" ] #Check if container named cmaketask is running.
 then
  docker container stop cmaketask #If container cmaketask already running then first it will be stopped.
 fi
docker rm cmaketask > /dev/null
fi
docker run --name cmaketask -ti -u $(id -u):$(id -g) -v "$(pwd)":/usr/src/cmaketask cmaketask:1.0