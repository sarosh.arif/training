






# Using sys library
# from __future__ import division #To get float division in circle_count/square_count
# import random
# import json
# import sys #Passing arguments to script
# import getopt #Using options in arguments

# def approx_pi(iterations):
#     circle_count=0
#     square_count=0
#     for count in range(int(iterations)):
#         x=random.uniform(-1,1) #x-axis value of random point
#         y=random.uniform(-1,1) #y-axis value of random point
#         d=x*x+y*y
#         if d<=1: #Check if random point inside of unit circle
#             circle_count+=1 #Increament circle count by 1
#         square_count+=1 #Increament Square count by 1
#     result=4*(circle_count/square_count)
#     print "Approx value of pi = "+str(result)

# def print_help():
#     print """This script calculates the approximate value of pi using Monte Carlo's simulation.\nUsage: <arg1> <arg2>\narg1=flag\narg2=argument required with flag
#         -i: Takes the no of iterations from user.
#         -j: Takes the no of iterations from JSON file.
#         -h: Provides help regarding this script."""

# #sys.argv gives arguments passed to script. Ignoring first argument as it is
# #name of the script
# try:
#     options,arguments=getopt.getopt(sys.argv[1:],"hji:") #options=>List of tuples arguments=>List
# except getopt.GetoptError: #Generates error when option which needs arguments has none or unknown option is used.
#     print "Error: Incorrect syntax."
#     print_help()
#     sys.exit(2) # 2 used because UNIX uses 2 for syntax error

# for option,argument in options: #options is a list of tuples containing each option with its argument.
#     if option=='-h':
#         print_help()
#     elif option=='-i':
#         if int(argument)>=0: #Check if iterations are positive or not.
#             approx_pi(argument)
#         else:
#             print "Error: Iterations can not be negative."
#     elif option=='-j':
#         jsonfile=open("json.json","r")
#         approx_pi(json.load(jsonfile)["Iterations"])