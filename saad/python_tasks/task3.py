from __future__ import division #To get float division

import subprocess 
#gives you a way to cleanly integrate shell commands into
#your scripts while managing input/output in a standard way.

import os #For directory


#CPU_detail=(os.system("lscpu"))#((subprocess.check_output("lscpu", shell=True).strip()).decode())#Gives CPU Details
#free command gives info about ram in Kilo bytes
#RAM=(subprocess.check_output("free -g | awk 'NR==2 {print $2}'", shell=True).strip()).decode()
if os.path.isdir("/home/saadkhan/Details"): #Check if Directory exists
    pass #Do nothing
else: #Creating directory Details in /home/saadkhan
    os.mkdir("/home/saadkhan/Details")
os.chdir("/home/saadkhan/Details")
os.system("lscpu > summary.txt") #Writing Details to text file
os.system('printf "RAM:\t\t  " >> summary.txt')
os.system('printf "\b\b\b" >> summary.txt')
os.system("free -g | awk 'NR==2 {print $2}' >> summary.txt") #Writing RAM detail to file.
