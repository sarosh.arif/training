infile=open("task5.txt","r") #Task5.txt is input file
input=infile.read()
inlist=input.split("\n")
no_of_words=0 #Counts total no of words
no_of_times=[] #List containing no of times each word is repeated.
listindex=0 #Index of input list
rangelist=range(int(inlist[0])+1) #rangelist=no of time the for loop should run
for nooftimes in rangelist:
    if inlist[listindex]!="" and not(str(inlist[listindex]).isdigit()): #if word is found
        no_of_words+=1
        no_of_times.append(inlist.count(inlist[listindex])) #Adding count of each word to list "no_of_times"
        rmword=inlist[listindex] #temporarily storing word which needs to be removed
        for count in range(inlist.count(inlist[listindex])):
            inlist.remove(rmword) #Remove word from list once its counted
            if count >= 1:
                rangelist.remove(rangelist[-1])
        listindex-=1 #Decreament list index as the word is removed
    listindex+=1 #Increament list index
print no_of_words
for index  in range(len(no_of_times)): #Printing each value of list "no_of_times"
    print no_of_times[index],