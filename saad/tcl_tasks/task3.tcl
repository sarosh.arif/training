#!bin/tclsh
proc fibo {input} {
    set curr_value 0
    set prev_value 1
    for {set iter 0} {$iter<$input} {incr iter} {
        set temp $curr_value
        set curr_value [expr $curr_value+$prev_value]
        set prev_value $temp
    }
    return $curr_value
}

set input [gets stdin]
puts "Result = [fibo $input]"

# set x 3
# puts {"Hello $x bye"}