#!bin/tclsh
# proc binary {num} {
#     if {$num==0} {
#         puts -nonewline 0 
#     } else {
#         if {$num==1} {
#             puts -nonewline 1
#             } else {
#                 binary [expr $num/2]
#                 puts -nonewline [expr $num%2]
#                 append temp [expr $num%2]
#             }
#     }
# }
# binary 0
# puts ""

proc binary {num} {
    if {$num==0} {
        set result 0
    } else {0
        while {$num>1} {
            append result [expr $num%2]
            set num [expr $num/2]
        }
        append result 1
    }
    return [string reverse $result]
}
puts [binary 6]