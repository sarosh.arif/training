#!bin/tclsh
for {set num 1} {$num<=100} {incr num} {
    if {[expr $num%3]==0 && [expr $num%5]==0} {
        puts "OnOff"
    } elseif {[expr $num%3]==0} {
        puts "On"
    } elseif {[expr $num%5]==0} {
        puts "Off"
    } else {
        puts $num
    }
}