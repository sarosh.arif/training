#!bin/tclsh
proc singledigit {num} {
    set result 0
    #Without [expr $num/10] > 0, result of numbers, which are multiples of 10, is 0.
    #While loop runs as long as remainder & division result is > 0.
    #In result, the remainder is added and the num variable is modified to new value.
    while {[expr $num%10] > 0 || [expr $num/10] > 0} {
        set result [expr $result + $num%10]
        set num [expr $num/10]
    }
    if {$result >=10} {
        singledigit $result
    } else {
        return $result
    }
}
proc main {} {
    puts -nonewline "Result = "
    puts [singledigit 1]
}
main