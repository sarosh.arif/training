#!bin/tclsh
set pattern for
set path /home/saadkhan/Training/Pythonfiles/python_tasks/*
#glob is used to find all files at the given path.
#filepath has the path of each file.
foreach filepath [glob -type f $path] {
    set fileopen [open $filepath] ;#Opening file using filepath. By default access mode is r.
    set filedata [read $fileopen] ;#Reading data from opened file.
    #* is used before and after pattern inorder to ignore the characters before and after the pattern
    if {[string match *$pattern* $filedata]} {
        puts "Pattern found in: $filepath"
    }
    close $fileopen ;#Closing opened files
}

#Alternate by running bash commands
#puts [exec grep -r -l pattern path]