#!bin/tclsh
proc rstring {input} {
    #for loop starts from last index of string.
    #if -1 is removed from set index. code works the same.
    for {set index [expr [string length $input]-1]} {$index>=0} {incr index -1} { ;#-1 is the step (Decreasing by 1)
        # set output(index) input(lengthofinput-index+1)
        append output [string index $input $index] ;#each character of string is added to output.
    }
    return $output
}
puts [rstring "abc def"]