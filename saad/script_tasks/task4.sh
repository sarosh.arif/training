#!/bin/bash

if [ $# -ne 1 ]
then
echo "Error: Incorrect no of arguments."
echo "Usage $(basename $0) <path>"
exit 1
fi

backuppath=$1 #The directory from which .c files are backed up.
#-d Checks directory's existance
if [ -d /home/backuptask4 ]
 then
 echo -e "\nBackup Directory exists."

 find $backuppath -type f -name "*.c"|while read filename; do #While starts. while takes one input from find at a time
 #filename = each file in path entered by user
 #-e Checks file's existance
 if [ -e /home/backuptask4/$(basename $filename) ] #Finding file in backup folder
  then
  #Excecutes when file copy exists in backup
  #Comparing files located in both backup folder and in path folder
  diff $filename /home/backuptask4/$(basename $filename) > /dev/null #No output printed to terminal

  #$?(exit code) = 0 diff command has no output
  #$?(exit code) = 1 diff command has output
  if [ $? -eq 0 ]
   then # : is no-op command in shell
   : #echo "No difference detected in files $(basename $filename)."
  else
   echo "Difference detected in $(basename $filename)"
   #Copy updated file from path to backup
   cp -p $filename /home/backuptask4/ #$(basename $filename)
   echo "File $(basename $filename) updated"
  fi

 else
 #Excecutes when file copy doesn't exist in backup
  cp -p $filename /home/backuptask4/
  echo "File $(basename $filename) had no previous copy. It is now backed up."
 fi

 done #While ends
else
 mkdir /home/backuptask4
 echo -e "\nBackup Directory created."
 find $backuppath -type f -name "*.c" -exec cp -p {} /home/backuptask4 \;
fi

#-type f selects files, -exec want to do some more complex things
