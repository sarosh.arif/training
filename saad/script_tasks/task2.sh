#!/bin/bash

if [ $# -ne 2 ]
then
echo "Error: Incorrect no of arguments."
echo "Usage $(basename $0) <row> <column>"
exit 1
fi

row=$1 #Argument 1
column=$2 #Agrument 2

output=$(awk -F ',' 'NR=='"$row"' { print $'"$column"'}' task2check.csv)

#-F ',' used to define field separator
#NR is the row number (no of records. Records=lines)
#NF is the column number (no of fields in a record)

if [ -z "$output" ]
then
echo "Empty cell"
else
echo "Data = $output"
fi
