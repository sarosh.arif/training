#!/bin/bash

if [ $# -ne 2 ]
then
echo "Error: Incorrect no of arguments."
echo "Usage $(basename $0) <pattern> <path>"
exit 1
fi

pattern=$1 #Argument 1
path=$2 #Agrument 2

#Checks if file exists or not. If it exists then deletes the existing file and creates a new file.
if [ -e "task1.txt" ] 
then
rm task1.txt
touch task1.txt
else
touch task1.txt
fi

line=$(grep -rhn $pattern $path) 
#-h Suppress the prefixing of file names on output, -l gives file name only, r recursive, n line numbers.

#Check if pattern is found or not.
if [ -z "$line" ]
 then
 echo "Pattern: $pattern not found in any file."
 rm task1.txt
else
 for pathout in $(grep -rl $pattern $path); do #Runs for all paths where pattern is found
  for lineout in $(grep -rhn $pattern $pathout); do #Runs for all line numbers at which pattern is found. Pathout used instead of path
   echo "$lineout">>task1.txt
  done
  echo "Path: $pathout">>task1.txt
  echo -e "Filename: $(basename $pathout)\n">> task1.txt
 done
fi
