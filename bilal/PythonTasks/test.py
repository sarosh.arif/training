import sys, getopt
import random as r
import math as m
import json

def help():
    print "\n Enter -i for Entering the value of iterations"
    print "\n Enter -j to read the value from JSON file"
    print "\n Enter -h for help"
    print "\n Usage: test.py -i <Number of Iterations> -j -h \n\n"
    return 0

def jsonfile():
    # read file
    with open('/home/emumba/Documents/Python/PythonTasks/iterations.json', 'r') as myfile:
        data=myfile.read()
    # parse file
    obj = json.loads(data)
    # show values
    total=obj["iteration"]
    print(obj["iteration"])
    return int(total)

def Estimating(total):
    inside = 0
    # Total number of darts to throw.
# Iterate for the number of darts.
    for i in range(0, total):
    # Generate random x, y in [0, 1].
        x2 = r.random()**2
        y2 = r.random()**2
    # Increment if inside unit circle.
        if m.sqrt(x2 + y2) < 1.0:
            inside += 1
# inside / total = pi / 4
    Estimated_pi = (float(inside) / total) * 4
    print('Estimated Value of pi = ' + str(Estimated_pi))
    return 0

options, arguments = getopt.getopt(sys.argv[1:],'jhi:')
for opt, arg in options:
    if opt == '-i' and int(arg)>0:
        total=int(arg)
        Estimating(total)
    elif opt == '-h':
        print "\t\thelp instructions"
        help()
        continue
    elif opt == '-j':
        total = jsonfile()
        Estimating(total) 
    else:
        print("\n\t\tWrong Input.....")
