#!/bin/bash

#Task2
echo

row=$1
column=$2

awk -F "," 'NR=='"$row"' {print $'"$column"'}' /home/emumba/Documents/Tasks/Task2Data.csv


#-F is a delimiter ","
#NR is telling row
#after Print and $ we enter Column number
#awk -F "," '{if(NR=='"$row"') print $'"$column"' }' /home/emumba/Documents/Tasks/Task2Data.csv
