#!/bin/bash

#Task4
SourceDir=$1
Destination=/home/backup/

if [ ! -d $Destination ]; then						#Checking if backup directory is already created or not
echo -e " \n\t Creating new Backup Directory....\n"

mkdir $Destination							#Creating new backup directory if not Al ready created
find $SourceDir -name '*.c' -exec cp -r {} $Destination \;		#Finfing .c Files in user Entered Directory and taking their Backup in backup directory
sleep 1									#Making it user friendly
echo -e " \n\t Backup Created Succesfully\n"


elif [ -d $Destination ]; then						#else if backup directory is already present


echo -e " \n\t Welcome Back....\n"
for i in $(find $SourceDir -name '*.c'); do				#running a for loop on the .c files is the user Entered Directory
filename=$(basename $i)							# Extracting file name from file Path

if [ -z $(find $Destination -name "$filename"'') ]; then 		#here -z check if the variable is Empty. Here checking if file is not present in Destination
  cp -a $i $Destination
  echo -e " $filename  File had no previous copy and is now backed up\n"    
  else  

	if(cmp --silent $Destination$filename $SourceDir$filename)then
	echo -e "Latest version is already present of $filename\n"; 
	else  
	cp -a $i $Destination
	echo -e " $filename has been updated\n"; 
	fi
fi

done

fi
