#!/usr/bin/env bash
#To avaoid any error we can remove already created container named by "task"
docker rm task 2> /dev/null
#To do the volume maping of our project directory into our container
docker run --name task -it -v $(pwd):/usr/src/mytasks cmdoctask