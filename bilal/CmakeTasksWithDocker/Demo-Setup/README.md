# Introduction

This project is using googletest and Benchmark Library and will generate 3 Executable (main,bench,mytest) in bin folder of project directory.

# Downloading

You can clone the repository by running git clone command and then go to the project directory by using cd command in shell, as shown below.
```shell
    git clone https://gitlab.com/sarosh.arif/training.git
    cd training/bilal/CmakeTasksWithDocker/Demo-Setup
```
# Building
 The Image "cmdoctask" will be build by running the following script file in shell
 
```shell
    ./BuildingImage.sh
```
# Running
 ### Running container
   Now you can run the container by executing the following scrip file in project directory
   
 ```shell
    ./RunningDockerContainer.sh
 ```
### Running cmake and make commands
To run cmake and make commands be  sure you are in /usr/src directory and run the following script file in shell
    
 ```shell
     ./mytasks/RunCmdinContainer.sh
 ```
 This will also display the 3 Executable (main,bench,mytest)
### (Optional) Running cmake and make commands manually in Container
 To get a Detailed picture you can run cmake and make results manually (i.e without using RunCmdinContainer.sh file mentioned in above heading)
 For this purpose you can run the following commands in shell(you must be in "/usr/src" directory )
 ```shell
     rm -r build bin mytasks/third_party_lib/benchmark mytasks/third_party_lib/googletest && mkdir build bin 2> /dev/null
     cp -r benchmark googletest mytasks/third_party_lib/
     cd mytasks/build
     cmake -DCMAKE_BUILD_TYPE=Release ..
     make
 ```
 Now you can run the executable created in "/bin" folder of project directory
 