set ( PROJECT_LINK_LIBS libbenchmark.a )
link_directories( ${CMAKE_BINARY_DIR}/third_party_lib/benchmark/src )

include_directories(${CMAKE_SOURCE_DIR}/third_party_lib/benchmark/include)
set (source student_benchmark.cpp ${CMAKE_SOURCE_DIR}/src/Student.cpp)
add_executable(bench ${source})
set_target_properties(bench PROPERTIES RUNTIME_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/bin)
target_link_libraries(bench ${PROJECT_LINK_LIBS} pthread )