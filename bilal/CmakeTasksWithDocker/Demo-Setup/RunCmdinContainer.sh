#!/usr/bin/env bash
rm -r mytasks/build mytasks/bin mytasks/third_party_lib/benchmark mytasks/third_party_lib/googletest 2> /dev/null
mkdir mytasks/build/ mytasks/bin/
cp -r benchmark googletest mytasks/third_party_lib/
cd mytasks/build
cmake -DCMAKE_BUILD_TYPE=Release ..
make

#now for checking the outputs of executable files in bin/ 
cd .././bin/
echo -e " \n\n\t\t ***Running the main Executable*** \n\n"
./main
echo -e " \n\n\t\t ***Running the bench Executable*** \n\n"
./bench
echo -e " \n\n\t\t ***Running the mytest Executable*** \n\n"
./mytest