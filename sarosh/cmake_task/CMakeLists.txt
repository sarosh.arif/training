
# root 
cmake_minimum_required(VERSION 3.10.2)
project(task)
include_directories(include)

set( CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib )
add_library(Student SHARED src/Student.cpp)

add_subdirectory(third_party_lib)
add_subdirectory(src)
add_subdirectory(benchmarks)
add_subdirectory(tests)


