# INTRODUCTION
--------------
To clone the folder use the following command: 
```bash
git clone --recursive https://gitlab.com/sarosh.arif/training
```

This task builds a docker image using a script named buildDocker and runs the docker container using a script named runDocker. 
Once inside the container the files are present at the following address: /usr/src.

# BUILDING
-------------
To build the docker image run the script named buildDocker placed in the cmake_task directory.

# RUNNING
-------------   
To run the container run the script named runDocker placed in the cmake_task directory.


## Run cmake and make
Follow these steps:
1. Create and go to the build directory 
2. Enter the following commands:

```bash
mkdir build
cd build
cmake ..
make
```
## File placement
All the libraries are placed in build/lib.
The following required executables are placed in build/bin:
1. mainapp
2. googletest
3. benchmarks 
  


