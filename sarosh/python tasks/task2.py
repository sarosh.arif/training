import math # in order to use sqrt


# taking input
print ("Please enter the following data: \n")

print ("UP: ")
up= int(input())

print ("DOWN: ")
down= int(input())

print ("LEFT: ")
left= int(input())

print ("RIGHT: ")
right= int(input())

vertical= up-down
horizonal= left-right

# pythagoras theorem to find the least distance and printing it as int to avoid decimals.
print ("least distance: ")
print ( round(math.sqrt(vertical**2 + horizonal**2)) )