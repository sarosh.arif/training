import random as r
import math as m
import json
import sys

if len(sys.argv) > 1:
    if str(sys.argv[1]) == '-h':
        print("\n Estimates the value of pi. \n")
        print("HELP: \n OPTIONS \n \t -h: Help \n \t -i <number>: Does the number of iterations provided \n \t -j: Reads the number of iterations from a .json file.\n")

    elif str(sys.argv[1]) == '-i':
        total= int(sys.argv[2])

    elif str(sys.argv[1]) == '-j':
        with open('JSON.json', 'r') as myfile:
            data=json.load(myfile)
            total=data['value']
        
    else:
        print("wrong format")


    #Number of darts that land inside.
    inside = 0
   
    if str(sys.argv[1]) != '-h' and total > 0:
        # Iterate for the number of darts.
        for i in range(0,total):
            # Generate random x, y in [0, 1].
            x2 = r.random()**2
            y2 = r.random()**2
            # Increment if inside unit circle
            if m.sqrt(x2 + y2) < 1.0:
                inside += 1

        # inside / total = pi / 4
        pi = (float(inside) / total) * 4

        print(pi)
else:
    print("--------------------------\n Wrong Format\n USE -h option for help\n--------------------------")