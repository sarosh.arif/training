#Byte Order:          Little Endian
#core(s) per socket:  4
#Socket(s):           1
#Model name:          Intel(R) Core(TM) i7-8550U CPU @ 1.80GHz
#CPU MHz:             1638.462
#CPU max MHz:         4000.0000
#CPU min MHz:         400.0000
#Virtualization Support:      VT-x
#L1           32K
#L2 cache:            256K
#L3 cache:            8192K
#RAM Memory: 15794MB
import os.path
import getpass
import os
import sys

username = getpass. getuser()

if os.path.exists("/home/" + username + "/Details"):
    print (" Details directory exists ")
else:
    os.mkdir ("/home/" + username + "/Details")


fileobj= open("/home/" + username + "/Details" + "/Summary.txt", "a")

os.system("lscpu > /home/" + username + "/Details/Summary.txt")

meminfo = dict((i.split()[0].rstrip(':'),int(i.split()[1])) for i in open('/proc/meminfo').readlines())
mem_kib = meminfo['MemTotal']
fileobj.write("RAM Memory: " + str(mem_kib/1024) + "MB\n")

fileobj.close()

