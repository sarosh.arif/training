#!/bin/bash

cfiles='/*.c'
test -d "/home/emumba/backup" && let check=1 || let check=0

# if the backup directory does not exist it creates a new directory names backup and copies all the .c files into the backup folder
if (($check==0));then
  mkdir /home/emumba/backup
  
  cp -a $1$cfiles /home/emumba/backup 
  echo "Backup Created"

fi

# if the backup folder already exists check if it has the same files
if (($check==1));then
 checking_modification=0
 cp -u $1$cfiles cf
 echo
 diff <(cd cf && find . | sort) <(cd /home/emumba/backup && find . | sort) > diffstatus
 
   if [ $? -eq 1 ];then
   
      echo "file had no previous copy and is now backed up"     #if files differ this message is displayed
      echo   
      checking_modification=1
 
  else 
   {
    echo
   }
  fi


 diff cf /home/emumba/backup
 cpstatus=$?

   if [ $cpstatus -eq 0 ];then
     echo "Files are the same. No updates required"   # if no chages have been made this message is displayed
     echo
   fi

   if [[ $checking_modification -eq 0 && $cpstatus -eq 1 ]];then
     echo "File has been updated"  #if the data within the files is changed then this message is displayed
     echo
   fi

 cp -r -u $1$cfiles /home/emumba/backup


fi
