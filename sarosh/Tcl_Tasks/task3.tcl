
gets stdin n
set fib1 0
set fib2 1
for {set i 0} {$i < $n} {incr i} {
    set fib3 [expr {$fib1 + $fib2}]
    set fib1 $fib2
    set fib2 $fib3

}
puts $fib1

