

proc sum_of_digits {num} {
   if { $num == 0} {
       return 0
   }
   return [expr $num % 10 + [sum_of_digits [expr $num / 10]]]
}




gets stdin number
for {set dig [sum_of_digits $number]} { $dig >= 10 } {} {
    set dig [sum_of_digits $dig]
    }

puts $dig

