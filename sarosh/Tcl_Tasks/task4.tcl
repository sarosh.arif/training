


for { set l 1 }  {$l <= 100} {incr l} {
    if { [expr $l % 5] == 0 && [expr $l % 3] == 0 } {
    # if condition is true then print the following 
    puts "OnOff"
    } elseif { [expr $l % 5] == 0 } {
    # if else if condition is true 
    puts "Off"
    } elseif { [expr $l % 3] == 0 } {
    # if else if condition is true 
    puts "On"
    } else {
    # if none of the conditions is true 
    puts $l
    }
}
